# coding=utf-8
"""Module with definitions for the Universal Robots UR10.

Joint axes are positive going from the black gasket through the joint
towards the light blue cap.

Information about coordinate systems:

 * Base: The z-direction is positive up through the base link. The y-direction is
positive along the cable going out of the base.

 * Tool flange: The z-direction is directed along the symmetry axis and is positive away from the robot. The y-direction is negative in the direction of the electrical connector socket.

For identification of link transforms, the following is important information about the pose of the robot with all joints 0:

 * Joint 1 is lying with the blue cap pointing in the negative y-direction of the base.

 * Joint 1 lies the robot down horizontaly, pointing in the negative x-direction of the base.

 * Joint 2 is link 2 and 3 parallel and outstretched.

 * Joint 3 holds link 4 towars the negative y-direction of the base.

 * Joint 3 points the light blue cap of link 4 in the positive z-direction of the base.

 * Link 4 holds link 5 in the positive z-direction of the base

 * Joint 4 point the blue cap of link 5 in the positive y-direction of the base.

 * The tool z-direction is in the negative y-direction of the base.

 * The tool y-direction is in the positive z-direction of the base.

There are three types of mechanical joint: Two shoulder joints, one elbow joint, three wrist joints. The joints have an input flange, with an aluminum gasket, and an output flange, with a black rubber gasket. Important observations:

 * The shoulder joints are mounted with their output flange inwards in the kinematic chain; i.e. they manipulate their own housing.

 * The shoulder and elbow joints have Their axis of rotation going from the centre of the black gasket towards the blue cap.

 * The wrist joints have their axis of rotation going from the blue cap towards the black gasket.
"""

__author__ = "Morten Lind"
__copyright__ = "SINTEF Manufacturing 2017-2021"
__credits__ = ["Morten Lind"]
__license__ = "LGPLv3"
__maintainer__ = "Morten Lind"
__email__ = "morten.lind@sintef.no"
__status__ = "Development"


import numpy as np
import math3d as m3d

from pymoco.kinematics import joints
from pymoco.robots.ur_base import UR_Base


class UR10(UR_Base):

    def __init__(self, **kwargs):
        UR_Base.__init__(self, **kwargs)

        # Internal link transform for the seven robot parts. The
        # explanation assumes a robot pose as the one shown in the
        # UR10 working area note.

        self._link_xforms = [m3d.Transform() for i in range(7)]

        # Link 0 is the base plate. The in-frame is at the bottom centre
        # of the plate with the z-direction up through the plate and
        # the y-direction out through the connector. The out-frame is
        # at the top of the plate.
        self._link_xforms[0]._v.z = 0.043

        # Link 1 out-frame is rotated around the x-direction to point
        # through joint 1. This brings the y-direction upwards.
        self._link_xforms[1]._o.rotate_xt(np.pi / 2)
        self._link_xforms[1]._o.rotate_zt(np.pi / 2)
        self._link_xforms[1]._v.y = -0.176 / 2  # = -0.088
        self._link_xforms[1]._v.z = 0.128 - self._link_xforms[0]._v.z
        # = 0.085

        # Link 2 does not change the orientation.
        self._link_xforms[2]._v.y = 0.612
        self._link_xforms[2]._v.z = 0.176 / 2 - 0.128 / 2  # = -0.088

        self._link_xforms[3]._v.y = 0.572
        self._link_xforms[3]._v.z = - 0.128 / 2 + 0.116 / 2  # = -0.006

        self._link_xforms[4]._o.rotate_yt(-np.pi / 2)
        self._link_xforms[4]._v.x = -0.116 / 2  # = 0.058
        self._link_xforms[4]._v.z = 0.116 / 2  # = 0.058

        self._link_xforms[5]._o.rotate_yt(np.pi / 2)
        self._link_xforms[5]._v.x = 0.116 / 2  # = 0.058
        self._link_xforms[5]._v.z = 0.116 / 2  # = 0.058

        self._link_xforms[6]._v.z = 0.092 - 0.116 / 2  # = 0.034

    def get_joint_xforms(self):
        return [joints.RevoluteJoint() for i in range(6)]
    joint_xforms = property(get_joint_xforms)
