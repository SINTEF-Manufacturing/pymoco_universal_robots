# coding=utf-8
"""
Module with definitions for the Universal Robots UR10 based on DH-parameters.
"""

__author__ = "Morten Lind"
__copyright__ = "SINTEF Manufacturingx 2021"
__credits__ = ["Morten Lind"]
__license__ = "LGPLv3"
__maintainer__ = "Morten Lind"
__email__ = "morten.lind@sintef.no"
__status__ = "Development"


import numpy as np
import math3d as m3d

from pymoco.kinematics import joints
from .ur_base import UR_Base


class UR10_DH(UR_Base):

    def __init__(self, **kwargs):
        UR_Base.__init__(self, **kwargs)

        # Nominal DH-parameters from controller:
        self._as = [0.0,
                    -0.612,
                    -0.5723,
                    0.0,
                    0.0,
                    0.0]
        self._ds = [0.1273,
                    0.0,
                    0.0,
                    0.163941,
                    0.1157,
                    0.0922]
        self._alphas = [np.pi/2, 0, 0, np.pi/2, -np.pi/2, 0]

        # Internal link transform for the seven robot parts.
        # All link transforms are inherently packed in the dh-joint transforms
        self._link_xforms = [m3d.Transform() for i in range(7)]
        # Exceptions are the base link transform and the tool flange
        # transform (?)
        # self._link_xforms[0] = m3d.Transform()
        # self._link_xforms[0]._v.z = 0.021
        # _link_xforms[6] = m3d.Transform()
        # _link_xforms[6]._v.z = 0.032785

    def get_joint_xforms(self):
        return [joints.DHRevJoint(*pars) for pars in
                zip(self._as, self._ds, self._alphas)]
    joint_xforms = property(get_joint_xforms)
